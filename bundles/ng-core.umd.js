(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('@angular/common'), require('@angular/platform-browser'), require('rxjs/operators'), require('@angular/common/http'), require('luxon')) :
    typeof define === 'function' && define.amd ? define('ng-core', ['exports', '@angular/core', 'rxjs', '@angular/common', '@angular/platform-browser', 'rxjs/operators', '@angular/common/http', 'luxon'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['ng-core'] = {}, global.ng.core, global.rxjs, global.ng.common, global.ng.platformBrowser, global.rxjs.operators, global.ng.common.http, global.luxon));
}(this, (function (exports, i0, rxjs, common, platformBrowser, operators, i1, luxon) { 'use strict';

    function _interopNamespace(e) {
        if (e && e.__esModule) return e;
        var n = Object.create(null);
        if (e) {
            Object.keys(e).forEach(function (k) {
                if (k !== 'default') {
                    var d = Object.getOwnPropertyDescriptor(e, k);
                    Object.defineProperty(n, k, d.get ? d : {
                        enumerable: true,
                        get: function () {
                            return e[k];
                        }
                    });
                }
            });
        }
        n['default'] = e;
        return Object.freeze(n);
    }

    var i0__namespace = /*#__PURE__*/_interopNamespace(i0);
    var i1__namespace = /*#__PURE__*/_interopNamespace(i1);

    var ModalService = /** @class */ (function () {
        function ModalService() {
            this._onShow = new rxjs.Subject();
            this.onShow = this._onShow.asObservable();
            this._onHide = new rxjs.Subject();
            this.onHide = this._onHide.asObservable();
        }
        ModalService.prototype.show = function (content) {
            this._onShow.next(content);
        };
        ModalService.prototype.hide = function () {
            this._onHide.next();
        };
        return ModalService;
    }());
    ModalService.ɵprov = i0__namespace.ɵɵdefineInjectable({ factory: function ModalService_Factory() { return new ModalService(); }, token: ModalService, providedIn: "root" });
    ModalService.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    ModalService.ctorParameters = function () { return []; };

    var ModalDirective = /** @class */ (function () {
        function ModalDirective(el, svc) {
            this.svc = svc;
            this.el = el;
        }
        ModalDirective.prototype.ngOnInit = function () {
            var _this = this;
            this.el.nativeElement.addEventListener('click', function () { _this.onClick(); });
        };
        ModalDirective.prototype.onClick = function () {
            this.svc.show(this.modal);
        };
        return ModalDirective;
    }());
    ModalDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: '[modal]'
                },] }
    ];
    ModalDirective.ctorParameters = function () { return [
        { type: i0.ElementRef },
        { type: ModalService }
    ]; };
    ModalDirective.propDecorators = {
        modal: [{ type: i0.Input }]
    };

    var ModalComponent = /** @class */ (function () {
        function ModalComponent(svc, changes) {
            this.svc = svc;
            this.changes = changes;
        }
        ModalComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.svc.onShow.subscribe(function (content) { _this.onShow(content); });
            this.svc.onHide.subscribe(function () { _this.hide(); });
        };
        ModalComponent.prototype.onShow = function (content) {
            var _this = this;
            this.visible = true;
            this.changes.detectChanges();
            setTimeout(function () {
                _this.content.createEmbeddedView(content);
            });
        };
        ModalComponent.prototype.onContainerClick = function (e) {
            if (!e.path.find(function (t) { return t.className === 'content'; })) {
                this.hide();
            }
        };
        ModalComponent.prototype.hide = function () {
            this.visible = false;
        };
        return ModalComponent;
    }());
    ModalComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'core-modal',
                    template: "<div *ngIf=\"visible\" class=\"container\">\n  <div class=\"overlay\"></div>\n  <div (click)=\"onContainerClick($event)\" class=\"content-container\">\n    <div class=\"content\">\n      <ng-container #content></ng-container>\n    </div>\n  </div>\n</div>",
                    styles: [".container,.content-container{display:flex;position:absolute;top:0;left:0;width:100vw;height:100vh}.overlay{flex-grow:1;background:black;opacity:.6}.content-container{justify-content:center;align-items:center}.content{background:white;border-radius:5px;padding:20px}\n"]
                },] }
    ];
    ModalComponent.ctorParameters = function () { return [
        { type: ModalService },
        { type: i0.ChangeDetectorRef }
    ]; };
    ModalComponent.propDecorators = {
        content: [{ type: i0.ViewChild, args: ['content', { read: i0.ViewContainerRef },] }]
    };

    var NgCoreComponent = /** @class */ (function () {
        function NgCoreComponent() {
        }
        NgCoreComponent.prototype.ngOnInit = function () {
        };
        return NgCoreComponent;
    }());
    NgCoreComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'ng-core',
                    template: "<core-modal></core-modal>",
                    styles: [""]
                },] }
    ];
    NgCoreComponent.ctorParameters = function () { return []; };

    var NgCoreModule = /** @class */ (function () {
        function NgCoreModule() {
        }
        return NgCoreModule;
    }());
    NgCoreModule.decorators = [
        { type: i0.NgModule, args: [{
                    declarations: [ModalDirective, ModalComponent, NgCoreComponent],
                    imports: [
                        common.CommonModule,
                        platformBrowser.BrowserModule
                    ],
                    exports: [NgCoreComponent, ModalDirective, ModalComponent]
                },] }
    ];

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (Object.prototype.hasOwnProperty.call(b, p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, o) {
        for (var p in m)
            if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p))
                __createBinding(o, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    /** @deprecated */
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    /** @deprecated */
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    function __spreadArray(to, from, pack) {
        if (pack || arguments.length === 2)
            for (var i = 0, l = from.length, ar; i < l; i++) {
                if (ar || !(i in from)) {
                    if (!ar)
                        ar = Array.prototype.slice.call(from, 0, i);
                    ar[i] = from[i];
                }
            }
        return to.concat(ar || Array.prototype.slice.call(from));
    }
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, state, kind, f) {
        if (kind === "a" && !f)
            throw new TypeError("Private accessor was defined without a getter");
        if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver))
            throw new TypeError("Cannot read private member from an object whose class did not declare it");
        return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
    }
    function __classPrivateFieldSet(receiver, state, value, kind, f) {
        if (kind === "m")
            throw new TypeError("Private method is not writable");
        if (kind === "a" && !f)
            throw new TypeError("Private accessor was defined without a setter");
        if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver))
            throw new TypeError("Cannot write private member to an object whose class did not declare it");
        return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
    }

    var HttpService = /** @class */ (function () {
        function HttpService(http) {
            this.http = http;
            this.requests = 0;
            this._onRequest = new rxjs.Subject();
            this.onRequest = this._onRequest.asObservable();
            this._onResponse = new rxjs.Subject();
            this.onResponse = this._onResponse.asObservable();
        }
        HttpService.prototype.handleResponse = function () {
            this.requests--;
            if (!this.requests)
                this._onResponse.next();
        };
        HttpService.prototype.executeRequest = function (observable) {
            var _this = this;
            return new Promise(function (res, rej) {
                _this.requests++;
                _this._onRequest.next();
                observable
                    .pipe(operators.retry(3))
                    .subscribe(function (data) {
                    res(data);
                    _this.handleResponse();
                }, function (error) {
                    rej(error);
                    _this.handleResponse();
                });
            });
        };
        HttpService.prototype.get = function (url, headers) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.executeRequest(this.http.get(url, {
                            headers: headers
                        }))];
                });
            });
        };
        HttpService.prototype.head = function (url, headers) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.executeRequest(this.http.head(url, {
                            headers: headers
                        }))];
                });
            });
        };
        HttpService.prototype.getText = function (url, headers) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.executeRequest(this.http.get(url, {
                            headers: headers,
                            responseType: 'text'
                        }))];
                });
            });
        };
        HttpService.prototype.post = function (url, body, headers) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = headers || new i1.HttpHeaders();
                            headers = headers.append('Content-Type', 'application/json');
                            return [4 /*yield*/, this.executeRequest(this.http.post(url, body, {
                                    headers: headers
                                }))];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        HttpService.prototype.postUrlFormEncoded = function (url, body, headers) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = headers || new i1.HttpHeaders();
                            headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
                            return [4 /*yield*/, this.executeRequest(this.http.post(url, body, {
                                    headers: headers
                                }))];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        HttpService.prototype.delete = function (url, headers) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.executeRequest(this.http.delete(url, {
                                headers: headers
                            }))];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        return HttpService;
    }());
    HttpService.ɵprov = i0__namespace.ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(i0__namespace.ɵɵinject(i1__namespace.HttpClient)); }, token: HttpService, providedIn: "root" });
    HttpService.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    HttpService.ctorParameters = function () { return [
        { type: i1.HttpClient }
    ]; };

    var StorageProvider = /** @class */ (function () {
        function StorageProvider(key) {
            this.key = key;
        }
        return StorageProvider;
    }());

    var BrowserStorageProvider = /** @class */ (function (_super) {
        __extends(BrowserStorageProvider, _super);
        function BrowserStorageProvider(key, store) {
            var _this = _super.call(this, key) || this;
            _this.store = store;
            return _this;
        }
        BrowserStorageProvider.prototype.get = function () {
            return JSON.parse(this.store.getItem(this.key));
        };
        BrowserStorageProvider.prototype.set = function (value) {
            this.store.setItem(this.key, JSON.stringify(value));
        };
        BrowserStorageProvider.prototype.remove = function () {
            this.store.removeItem(this.key);
        };
        return BrowserStorageProvider;
    }(StorageProvider));

    var LocalStorageProvider = /** @class */ (function (_super) {
        __extends(LocalStorageProvider, _super);
        function LocalStorageProvider(key) {
            return _super.call(this, key, localStorage) || this;
        }
        return LocalStorageProvider;
    }(BrowserStorageProvider));

    var SessionStorageProvider = /** @class */ (function (_super) {
        __extends(SessionStorageProvider, _super);
        function SessionStorageProvider(key) {
            return _super.call(this, key, sessionStorage) || this;
        }
        return SessionStorageProvider;
    }(BrowserStorageProvider));

    var TimedStorageItem = /** @class */ (function () {
        function TimedStorageItem(value, expiration) {
            this.value = value;
            this.expiration = expiration;
        }
        return TimedStorageItem;
    }());

    var StorageUtils = /** @class */ (function () {
        function StorageUtils() {
        }
        StorageUtils.isExpired = function (item) {
            return luxon.DateTime.now().toSeconds() > item.expiration;
        };
        return StorageUtils;
    }());

    var TimedBrowserStorageProvider = /** @class */ (function (_super) {
        __extends(TimedBrowserStorageProvider, _super);
        function TimedBrowserStorageProvider(key, store) {
            return _super.call(this, key, store) || this;
        }
        TimedBrowserStorageProvider.prototype.get = function () {
            var item = JSON.parse(this.store.getItem(this.key));
            return (StorageUtils.isExpired(item) ? null : item.value);
        };
        TimedBrowserStorageProvider.prototype.set = function (value) {
            throw '"set" not supported. Use "setValue" instead.';
        };
        TimedBrowserStorageProvider.prototype.setValue = function (value, expiresIn) {
            var expiration = luxon.DateTime.now().plus({ seconds: expiresIn }).toSeconds();
            var item = new TimedStorageItem(value, expiration);
            this.store.setItem(this.key, JSON.stringify(item));
        };
        return TimedBrowserStorageProvider;
    }(BrowserStorageProvider));

    var TimedLocalStorageProvider = /** @class */ (function (_super) {
        __extends(TimedLocalStorageProvider, _super);
        function TimedLocalStorageProvider(key) {
            return _super.call(this, key, localStorage) || this;
        }
        return TimedLocalStorageProvider;
    }(TimedBrowserStorageProvider));

    var TimedSessionStorageProvider = /** @class */ (function (_super) {
        __extends(TimedSessionStorageProvider, _super);
        function TimedSessionStorageProvider(key) {
            return _super.call(this, key, sessionStorage) || this;
        }
        return TimedSessionStorageProvider;
    }(TimedBrowserStorageProvider));

    var ApiResponse = /** @class */ (function () {
        function ApiResponse() {
        }
        return ApiResponse;
    }());

    /*
     * Public API Surface of ng-core
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.ApiResponse = ApiResponse;
    exports.BrowserStorageProvider = BrowserStorageProvider;
    exports.HttpService = HttpService;
    exports.LocalStorageProvider = LocalStorageProvider;
    exports.ModalComponent = ModalComponent;
    exports.ModalDirective = ModalDirective;
    exports.ModalService = ModalService;
    exports.NgCoreComponent = NgCoreComponent;
    exports.NgCoreModule = NgCoreModule;
    exports.SessionStorageProvider = SessionStorageProvider;
    exports.StorageProvider = StorageProvider;
    exports.StorageUtils = StorageUtils;
    exports.TimedBrowserStorageProvider = TimedBrowserStorageProvider;
    exports.TimedLocalStorageProvider = TimedLocalStorageProvider;
    exports.TimedSessionStorageProvider = TimedSessionStorageProvider;
    exports.TimedStorageItem = TimedStorageItem;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ng-core.umd.js.map
