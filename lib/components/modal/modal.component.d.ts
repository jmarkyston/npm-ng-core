import { ChangeDetectorRef, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { ModalService } from '../../services/modal.service';
export declare class ModalComponent implements OnInit {
    private svc;
    private changes;
    content: ViewContainerRef;
    visible: boolean;
    constructor(svc: ModalService, changes: ChangeDetectorRef);
    ngOnInit(): void;
    onShow(content: TemplateRef<unknown>): void;
    onContainerClick(e: any): void;
    hide(): void;
}
