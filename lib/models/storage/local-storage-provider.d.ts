import { BrowserStorageProvider } from "./browser-storage-provider";
export declare class LocalStorageProvider<T> extends BrowserStorageProvider<T> {
    constructor(key: string);
}
