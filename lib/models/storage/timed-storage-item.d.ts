export declare class TimedStorageItem<T> {
    value: T;
    expiration: number;
    constructor(value: T, expiration: number);
}
