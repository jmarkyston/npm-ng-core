import { BrowserStorageProvider } from "./browser-storage-provider";
export declare class SessionStorageProvider<T> extends BrowserStorageProvider<T> {
    constructor(key: string);
}
