import { IStorageProvider } from "./storage-provider.i";
export declare abstract class StorageProvider<T> implements IStorageProvider<T> {
    protected key: string;
    constructor(key: string);
    abstract get(): T;
    abstract set(value: T): void;
    abstract remove(): void;
}
