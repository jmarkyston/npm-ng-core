import { TimedBrowserStorageProvider } from "./timed-browser-storage-provider";
export declare class TimedSessionStorageProvider<T> extends TimedBrowserStorageProvider<T> {
    constructor(key: string);
}
