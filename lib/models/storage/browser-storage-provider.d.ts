import { StorageProvider } from "./storage-provider";
export declare class BrowserStorageProvider<T> extends StorageProvider<T> {
    protected store: Storage;
    constructor(key: string, store: Storage);
    get(): T;
    set(value: T): void;
    remove(): void;
}
