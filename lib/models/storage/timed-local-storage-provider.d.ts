import { TimedBrowserStorageProvider } from "./timed-browser-storage-provider";
export declare class TimedLocalStorageProvider<T> extends TimedBrowserStorageProvider<T> {
    constructor(key: string);
}
