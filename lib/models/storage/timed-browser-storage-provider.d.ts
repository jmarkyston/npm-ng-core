import { BrowserStorageProvider } from "./browser-storage-provider";
export declare class TimedBrowserStorageProvider<T> extends BrowserStorageProvider<T> {
    constructor(key: string, store: Storage);
    get(): T;
    set(value: T): void;
    setValue(value: T, expiresIn: number): void;
}
