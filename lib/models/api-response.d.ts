export declare class ApiResponse<T> {
    data: T;
    error: string;
}
