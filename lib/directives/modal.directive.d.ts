import { ElementRef, OnInit, TemplateRef } from '@angular/core';
import { ModalService } from '../services/modal.service';
export declare class ModalDirective implements OnInit {
    private svc;
    modal: TemplateRef<unknown>;
    private el;
    constructor(el: ElementRef, svc: ModalService);
    ngOnInit(): void;
    private onClick;
}
