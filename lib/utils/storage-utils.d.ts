import { TimedStorageItem } from "../models/storage/timed-storage-item";
export declare class StorageUtils {
    static isExpired(item: TimedStorageItem<any>): boolean;
}
