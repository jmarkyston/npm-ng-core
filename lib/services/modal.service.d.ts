import { TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
export declare class ModalService {
    private _onShow;
    onShow: Observable<TemplateRef<unknown>>;
    private _onHide;
    onHide: Observable<void>;
    constructor();
    show(content: TemplateRef<unknown>): void;
    hide(): void;
}
