import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export declare class HttpService {
    private http;
    private requests;
    private _onRequest;
    onRequest: Observable<void>;
    private _onResponse;
    onResponse: Observable<void>;
    constructor(http: HttpClient);
    private handleResponse;
    private executeRequest;
    get<T>(url: string, headers?: HttpHeaders): Promise<T>;
    head<T>(url: string, headers?: HttpHeaders): Promise<T>;
    getText(url: string, headers?: HttpHeaders): Promise<string>;
    post<T>(url: string, body: any, headers?: HttpHeaders): Promise<T>;
    postUrlFormEncoded<T>(url: string, body: HttpParams, headers?: HttpHeaders): Promise<T>;
    delete(url: string, headers?: HttpHeaders): Promise<any>;
}
