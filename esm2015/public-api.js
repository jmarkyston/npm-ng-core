/*
 * Public API Surface of ng-core
 */
export * from './lib/ng-core.module';
export * from './lib/services/http.service';
export * from './lib/models/storage/browser-storage-provider';
export * from './lib/models/storage/local-storage-provider';
export * from './lib/models/storage/session-storage-provider';
export * from './lib/models/storage/storage-provider';
export * from './lib/models/storage/storage-provider.i';
export * from './lib/models/storage/timed-browser-storage-provider';
export * from './lib/models/storage/timed-local-storage-provider';
export * from './lib/models/storage/timed-session-storage-provider copy';
export * from './lib/models/storage/timed-storage-item';
export * from './lib/models/api-response';
export * from './lib/utils/storage-utils';
export * from './lib/services/modal.service';
export * from './lib/components/modal/modal.component';
export * from './lib/directives/modal.directive';
export * from './lib/components/ng-core/ng-core.component';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3Byb2plY3RzL25nLWNvcmUvc3JjL3B1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0dBRUc7QUFFSCxjQUFjLHNCQUFzQixDQUFDO0FBQ3JDLGNBQWMsNkJBQTZCLENBQUM7QUFDNUMsY0FBYywrQ0FBK0MsQ0FBQztBQUM5RCxjQUFjLDZDQUE2QyxDQUFDO0FBQzVELGNBQWMsK0NBQStDLENBQUM7QUFDOUQsY0FBYyx1Q0FBdUMsQ0FBQztBQUN0RCxjQUFjLHlDQUF5QyxDQUFDO0FBQ3hELGNBQWMscURBQXFELENBQUM7QUFDcEUsY0FBYyxtREFBbUQsQ0FBQztBQUNsRSxjQUFjLDBEQUEwRCxDQUFDO0FBQ3pFLGNBQWMseUNBQXlDLENBQUM7QUFDeEQsY0FBYywyQkFBMkIsQ0FBQztBQUMxQyxjQUFjLDJCQUEyQixDQUFDO0FBQzFDLGNBQWMsOEJBQThCLENBQUM7QUFDN0MsY0FBYyx3Q0FBd0MsQ0FBQztBQUN2RCxjQUFjLGtDQUFrQyxDQUFDO0FBQ2pELGNBQWMsNENBQTRDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIG5nLWNvcmVcbiAqL1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9uZy1jb3JlLm1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9zZXJ2aWNlcy9odHRwLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbW9kZWxzL3N0b3JhZ2UvYnJvd3Nlci1zdG9yYWdlLXByb3ZpZGVyJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9zdG9yYWdlL2xvY2FsLXN0b3JhZ2UtcHJvdmlkZXInO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbW9kZWxzL3N0b3JhZ2Uvc2Vzc2lvbi1zdG9yYWdlLXByb3ZpZGVyJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9zdG9yYWdlL3N0b3JhZ2UtcHJvdmlkZXInO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbW9kZWxzL3N0b3JhZ2Uvc3RvcmFnZS1wcm92aWRlci5pJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9zdG9yYWdlL3RpbWVkLWJyb3dzZXItc3RvcmFnZS1wcm92aWRlcic7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9tb2RlbHMvc3RvcmFnZS90aW1lZC1sb2NhbC1zdG9yYWdlLXByb3ZpZGVyJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9zdG9yYWdlL3RpbWVkLXNlc3Npb24tc3RvcmFnZS1wcm92aWRlciBjb3B5JztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9zdG9yYWdlL3RpbWVkLXN0b3JhZ2UtaXRlbSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9tb2RlbHMvYXBpLXJlc3BvbnNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWxzL3N0b3JhZ2UtdXRpbHMnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmljZXMvbW9kYWwuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb21wb25lbnRzL21vZGFsL21vZGFsLmNvbXBvbmVudCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9kaXJlY3RpdmVzL21vZGFsLmRpcmVjdGl2ZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb21wb25lbnRzL25nLWNvcmUvbmctY29yZS5jb21wb25lbnQnOyJdfQ==