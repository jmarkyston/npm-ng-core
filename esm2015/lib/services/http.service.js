import { __awaiter } from "tslib";
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class HttpService {
    constructor(http) {
        this.http = http;
        this.requests = 0;
        this._onRequest = new Subject();
        this.onRequest = this._onRequest.asObservable();
        this._onResponse = new Subject();
        this.onResponse = this._onResponse.asObservable();
    }
    handleResponse() {
        this.requests--;
        if (!this.requests)
            this._onResponse.next();
    }
    executeRequest(observable) {
        return new Promise((res, rej) => {
            this.requests++;
            this._onRequest.next();
            observable
                .pipe(retry(3))
                .subscribe((data) => {
                res(data);
                this.handleResponse();
            }, (error) => {
                rej(error);
                this.handleResponse();
            });
        });
    }
    get(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.executeRequest(this.http.get(url, {
                headers
            }));
        });
    }
    head(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.executeRequest(this.http.head(url, {
                headers
            }));
        });
    }
    getText(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.executeRequest(this.http.get(url, {
                headers,
                responseType: 'text'
            }));
        });
    }
    post(url, body, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            headers = headers || new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            return yield this.executeRequest(this.http.post(url, body, {
                headers
            }));
        });
    }
    postUrlFormEncoded(url, body, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            headers = headers || new HttpHeaders();
            headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            return yield this.executeRequest(this.http.post(url, body, {
                headers
            }));
        });
    }
    delete(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.executeRequest(this.http.delete(url, {
                headers
            }));
        });
    }
}
HttpService.ɵprov = i0.ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(i0.ɵɵinject(i1.HttpClient)); }, token: HttpService, providedIn: "root" });
HttpService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
HttpService.ctorParameters = () => [
    { type: HttpClient }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmctY29yZS9zcmMvbGliL3NlcnZpY2VzL2h0dHAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN2QyxPQUFPLEVBQXFCLFVBQVUsRUFBRSxXQUFXLEVBQWMsTUFBTSxzQkFBc0IsQ0FBQzs7O0FBSzlGLE1BQU0sT0FBTyxXQUFXO0lBU3RCLFlBQ1UsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQVRsQixhQUFRLEdBQVcsQ0FBQyxDQUFDO1FBRXJCLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBQ3pDLGNBQVMsR0FBcUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVyRCxnQkFBVyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFDMUMsZUFBVSxHQUFxQixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBSTNELENBQUM7SUFFRyxjQUFjO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVE7WUFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRU8sY0FBYyxDQUFJLFVBQXlCO1FBQ2pELE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkIsVUFBVTtpQkFDUCxJQUFJLENBQ0gsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUNUO2lCQUNBLFNBQVMsQ0FDUixDQUFDLElBQU8sRUFBRSxFQUFFO2dCQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDVixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUNELENBQUMsS0FBd0IsRUFBRSxFQUFFO2dCQUMzQixHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ1gsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FDRixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUssR0FBRyxDQUFJLEdBQVcsRUFBRSxPQUFxQjs7WUFDN0MsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLEdBQUcsRUFBRTtnQkFDbEQsT0FBTzthQUNSLENBQUMsQ0FBQyxDQUFDO1FBQ04sQ0FBQztLQUFBO0lBRUssSUFBSSxDQUFJLEdBQVcsRUFBRSxPQUFxQjs7WUFDOUMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFJLEdBQUcsRUFBRTtnQkFDbkQsT0FBTzthQUNSLENBQUMsQ0FBQyxDQUFDO1FBQ04sQ0FBQztLQUFBO0lBRUssT0FBTyxDQUFDLEdBQVcsRUFBRSxPQUFxQjs7WUFDOUMsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRTtnQkFDcEQsT0FBTztnQkFDUCxZQUFZLEVBQUUsTUFBTTthQUNyQixDQUFDLENBQUMsQ0FBQztRQUNOLENBQUM7S0FBQTtJQUVLLElBQUksQ0FBSSxHQUFXLEVBQUUsSUFBUyxFQUFFLE9BQXFCOztZQUN6RCxPQUFPLEdBQUcsT0FBTyxJQUFJLElBQUksV0FBVyxFQUFFLENBQUM7WUFDdkMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUM7WUFDN0QsT0FBTyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUksR0FBRyxFQUFFLElBQUksRUFBRTtnQkFDL0QsT0FBTzthQUNSLENBQUMsQ0FBQyxDQUFDO1FBQ04sQ0FBQztLQUFBO0lBRUssa0JBQWtCLENBQUksR0FBVyxFQUFFLElBQWdCLEVBQUUsT0FBcUI7O1lBQzlFLE9BQU8sR0FBRyxPQUFPLElBQUksSUFBSSxXQUFXLEVBQUUsQ0FBQztZQUN2QyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsbUNBQW1DLENBQUMsQ0FBQztZQUM5RSxPQUFPLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSxHQUFHLEVBQUUsSUFBSSxFQUFFO2dCQUMvRCxPQUFPO2FBQ1IsQ0FBQyxDQUFDLENBQUM7UUFDTixDQUFDO0tBQUE7SUFFSyxNQUFNLENBQUMsR0FBVyxFQUFFLE9BQXFCOztZQUM3QyxPQUFPLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUU7Z0JBQzFELE9BQU87YUFDUixDQUFDLENBQUMsQ0FBQztRQUNOLENBQUM7S0FBQTs7OztZQWxGRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7OztZQUoyQixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgcmV0cnkgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBIdHRwRXJyb3JSZXNwb25zZSwgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEh0dHBTZXJ2aWNlIHtcbiAgcHJpdmF0ZSByZXF1ZXN0czogbnVtYmVyID0gMDtcblxuICBwcml2YXRlIF9vblJlcXVlc3QgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xuICBvblJlcXVlc3Q6IE9ic2VydmFibGU8dm9pZD4gPSB0aGlzLl9vblJlcXVlc3QuYXNPYnNlcnZhYmxlKCk7XG5cbiAgcHJpdmF0ZSBfb25SZXNwb25zZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XG4gIG9uUmVzcG9uc2U6IE9ic2VydmFibGU8dm9pZD4gPSB0aGlzLl9vblJlc3BvbnNlLmFzT2JzZXJ2YWJsZSgpO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudFxuICApIHsgfVxuXG4gIHByaXZhdGUgaGFuZGxlUmVzcG9uc2UoKSB7XG4gICAgdGhpcy5yZXF1ZXN0cy0tO1xuICAgIGlmICghdGhpcy5yZXF1ZXN0cylcbiAgICAgIHRoaXMuX29uUmVzcG9uc2UubmV4dCgpO1xuICB9XG5cbiAgcHJpdmF0ZSBleGVjdXRlUmVxdWVzdDxUPihvYnNlcnZhYmxlOiBPYnNlcnZhYmxlPFQ+KTogUHJvbWlzZTxUPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXMsIHJlaikgPT4ge1xuICAgICAgdGhpcy5yZXF1ZXN0cysrO1xuICAgICAgdGhpcy5fb25SZXF1ZXN0Lm5leHQoKTtcbiAgICAgIG9ic2VydmFibGVcbiAgICAgICAgLnBpcGUoXG4gICAgICAgICAgcmV0cnkoMylcbiAgICAgICAgKVxuICAgICAgICAuc3Vic2NyaWJlKFxuICAgICAgICAgIChkYXRhOiBUKSA9PiB7XG4gICAgICAgICAgICByZXMoZGF0YSk7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZVJlc3BvbnNlKCk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICAoZXJyb3I6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICByZWooZXJyb3IpO1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVSZXNwb25zZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9KTtcbiAgfVxuXG4gIGFzeW5jIGdldDxUPih1cmw6IHN0cmluZywgaGVhZGVycz86IEh0dHBIZWFkZXJzKTogUHJvbWlzZTxUPiB7XG4gICAgcmV0dXJuIHRoaXMuZXhlY3V0ZVJlcXVlc3Q8VD4odGhpcy5odHRwLmdldDxUPih1cmwsIHtcbiAgICAgIGhlYWRlcnNcbiAgICB9KSk7XG4gIH1cblxuICBhc3luYyBoZWFkPFQ+KHVybDogc3RyaW5nLCBoZWFkZXJzPzogSHR0cEhlYWRlcnMpOiBQcm9taXNlPFQ+IHtcbiAgICByZXR1cm4gdGhpcy5leGVjdXRlUmVxdWVzdDxUPih0aGlzLmh0dHAuaGVhZDxUPih1cmwsIHtcbiAgICAgIGhlYWRlcnNcbiAgICB9KSk7XG4gIH1cblxuICBhc3luYyBnZXRUZXh0KHVybDogc3RyaW5nLCBoZWFkZXJzPzogSHR0cEhlYWRlcnMpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybiB0aGlzLmV4ZWN1dGVSZXF1ZXN0PHN0cmluZz4odGhpcy5odHRwLmdldCh1cmwsIHtcbiAgICAgIGhlYWRlcnMsXG4gICAgICByZXNwb25zZVR5cGU6ICd0ZXh0J1xuICAgIH0pKTtcbiAgfVxuXG4gIGFzeW5jIHBvc3Q8VD4odXJsOiBzdHJpbmcsIGJvZHk6IGFueSwgaGVhZGVycz86IEh0dHBIZWFkZXJzKTogUHJvbWlzZTxUPiB7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMgfHwgbmV3IEh0dHBIZWFkZXJzKCk7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24vanNvbicpO1xuICAgIHJldHVybiBhd2FpdCB0aGlzLmV4ZWN1dGVSZXF1ZXN0PFQ+KHRoaXMuaHR0cC5wb3N0PFQ+KHVybCwgYm9keSwge1xuICAgICAgaGVhZGVyc1xuICAgIH0pKTtcbiAgfVxuXG4gIGFzeW5jIHBvc3RVcmxGb3JtRW5jb2RlZDxUPih1cmw6IHN0cmluZywgYm9keTogSHR0cFBhcmFtcywgaGVhZGVycz86IEh0dHBIZWFkZXJzKTogUHJvbWlzZTxUPiB7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMgfHwgbmV3IEh0dHBIZWFkZXJzKCk7XG4gICAgaGVhZGVycyA9IGhlYWRlcnMuYXBwZW5kKCdDb250ZW50LVR5cGUnLCAnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyk7XG4gICAgcmV0dXJuIGF3YWl0IHRoaXMuZXhlY3V0ZVJlcXVlc3Q8VD4odGhpcy5odHRwLnBvc3Q8VD4odXJsLCBib2R5LCB7XG4gICAgICBoZWFkZXJzXG4gICAgfSkpO1xuICB9XG5cbiAgYXN5bmMgZGVsZXRlKHVybDogc3RyaW5nLCBoZWFkZXJzPzogSHR0cEhlYWRlcnMpIHtcbiAgICByZXR1cm4gYXdhaXQgdGhpcy5leGVjdXRlUmVxdWVzdDxhbnk+KHRoaXMuaHR0cC5kZWxldGUodXJsLCB7XG4gICAgICBoZWFkZXJzXG4gICAgfSkpO1xuICB9XG59XG4iXX0=