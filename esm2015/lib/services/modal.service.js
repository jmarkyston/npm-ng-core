import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
export class ModalService {
    constructor() {
        this._onShow = new Subject();
        this.onShow = this._onShow.asObservable();
        this._onHide = new Subject();
        this.onHide = this._onHide.asObservable();
    }
    show(content) {
        this._onShow.next(content);
    }
    hide() {
        this._onHide.next();
    }
}
ModalService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ModalService_Factory() { return new ModalService(); }, token: ModalService, providedIn: "root" });
ModalService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
ModalService.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25nLWNvcmUvc3JjL2xpYi9zZXJ2aWNlcy9tb2RhbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQWUsTUFBTSxlQUFlLENBQUM7QUFDeEQsT0FBTyxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFLM0MsTUFBTSxPQUFPLFlBQVk7SUFPdkI7UUFOUSxZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQXdCLENBQUM7UUFDdEQsV0FBTSxHQUFxQyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRS9ELFlBQU8sR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBQ3RDLFdBQU0sR0FBcUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUV2QyxDQUFDO0lBRWpCLElBQUksQ0FBQyxPQUE2QjtRQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsSUFBSTtRQUNGLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztZQWxCRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBUZW1wbGF0ZVJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBNb2RhbFNlcnZpY2Uge1xuICBwcml2YXRlIF9vblNob3cgPSBuZXcgU3ViamVjdDxUZW1wbGF0ZVJlZjx1bmtub3duPj4oKTtcbiAgb25TaG93OiBPYnNlcnZhYmxlPFRlbXBsYXRlUmVmPHVua25vd24+PiA9IHRoaXMuX29uU2hvdy5hc09ic2VydmFibGUoKTtcblxuICBwcml2YXRlIF9vbkhpZGUgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xuICBvbkhpZGU6IE9ic2VydmFibGU8dm9pZD4gPSB0aGlzLl9vbkhpZGUuYXNPYnNlcnZhYmxlKCk7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBzaG93KGNvbnRlbnQ6IFRlbXBsYXRlUmVmPHVua25vd24+KSB7XG4gICAgdGhpcy5fb25TaG93Lm5leHQoY29udGVudCk7XG4gIH1cblxuICBoaWRlKCkge1xuICAgIHRoaXMuX29uSGlkZS5uZXh0KCk7XG4gIH1cbn1cbiJdfQ==