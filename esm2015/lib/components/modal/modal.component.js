import { ChangeDetectorRef, Component, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalService } from '../../services/modal.service';
export class ModalComponent {
    constructor(svc, changes) {
        this.svc = svc;
        this.changes = changes;
    }
    ngOnInit() {
        this.svc.onShow.subscribe((content) => { this.onShow(content); });
        this.svc.onHide.subscribe(() => { this.hide(); });
    }
    onShow(content) {
        this.visible = true;
        this.changes.detectChanges();
        setTimeout(() => {
            this.content.createEmbeddedView(content);
        });
    }
    onContainerClick(e) {
        if (!e.path.find(t => t.className === 'content')) {
            this.hide();
        }
    }
    hide() {
        this.visible = false;
    }
}
ModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-modal',
                template: "<div *ngIf=\"visible\" class=\"container\">\n  <div class=\"overlay\"></div>\n  <div (click)=\"onContainerClick($event)\" class=\"content-container\">\n    <div class=\"content\">\n      <ng-container #content></ng-container>\n    </div>\n  </div>\n</div>",
                styles: [".container,.content-container{display:flex;position:absolute;top:0;left:0;width:100vw;height:100vh}.overlay{flex-grow:1;background:black;opacity:.6}.content-container{justify-content:center;align-items:center}.content{background:white;border-radius:5px;padding:20px}\n"]
            },] }
];
ModalComponent.ctorParameters = () => [
    { type: ModalService },
    { type: ChangeDetectorRef }
];
ModalComponent.propDecorators = {
    content: [{ type: ViewChild, args: ['content', { read: ViewContainerRef },] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmctY29yZS9zcmMvbGliL2NvbXBvbmVudHMvbW9kYWwvbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQXVCLFNBQVMsRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFPNUQsTUFBTSxPQUFPLGNBQWM7SUFLekIsWUFDVSxHQUFpQixFQUNqQixPQUEwQjtRQUQxQixRQUFHLEdBQUgsR0FBRyxDQUFjO1FBQ2pCLFlBQU8sR0FBUCxPQUFPLENBQW1CO0lBQ2hDLENBQUM7SUFFTCxRQUFRO1FBQ04sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsT0FBNkIsRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQsTUFBTSxDQUFDLE9BQTZCO1FBQ2xDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDN0IsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQyxFQUFFO1lBQ2hELElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELElBQUk7UUFDRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDOzs7WUFwQ0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxZQUFZO2dCQUN0QiwyUUFBcUM7O2FBRXRDOzs7WUFOUSxZQUFZO1lBRFosaUJBQWlCOzs7c0JBU3ZCLFNBQVMsU0FBQyxTQUFTLEVBQUUsRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDaGFuZ2VEZXRlY3RvclJlZiwgQ29tcG9uZW50LCBPbkluaXQsIFRlbXBsYXRlUmVmLCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1vZGFsU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL21vZGFsLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdjb3JlLW1vZGFsJyxcbiAgdGVtcGxhdGVVcmw6ICcuL21vZGFsLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbW9kYWwuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIE1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQFZpZXdDaGlsZCgnY29udGVudCcsIHsgcmVhZDogVmlld0NvbnRhaW5lclJlZiB9KSBjb250ZW50OiBWaWV3Q29udGFpbmVyUmVmO1xuXG4gIHZpc2libGU6IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBzdmM6IE1vZGFsU2VydmljZSxcbiAgICBwcml2YXRlIGNoYW5nZXM6IENoYW5nZURldGVjdG9yUmVmXG4gICkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgdGhpcy5zdmMub25TaG93LnN1YnNjcmliZSgoY29udGVudDogVGVtcGxhdGVSZWY8dW5rbm93bj4pID0+IHsgdGhpcy5vblNob3coY29udGVudCk7IH0pO1xuICAgIHRoaXMuc3ZjLm9uSGlkZS5zdWJzY3JpYmUoKCkgPT4geyB0aGlzLmhpZGUoKTsgfSk7XG4gIH1cblxuICBvblNob3coY29udGVudDogVGVtcGxhdGVSZWY8dW5rbm93bj4pIHtcbiAgICB0aGlzLnZpc2libGUgPSB0cnVlO1xuICAgIHRoaXMuY2hhbmdlcy5kZXRlY3RDaGFuZ2VzKCk7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLmNvbnRlbnQuY3JlYXRlRW1iZWRkZWRWaWV3KGNvbnRlbnQpO1xuICAgIH0pO1xuICB9XG5cbiAgb25Db250YWluZXJDbGljayhlKSB7XG4gICAgaWYgKCFlLnBhdGguZmluZCh0ID0+IHQuY2xhc3NOYW1lID09PSAnY29udGVudCcpKSB7XG4gICAgICB0aGlzLmhpZGUoKTtcbiAgICB9XG4gIH1cblxuICBoaWRlKCkge1xuICAgIHRoaXMudmlzaWJsZSA9IGZhbHNlO1xuICB9XG59XG4iXX0=