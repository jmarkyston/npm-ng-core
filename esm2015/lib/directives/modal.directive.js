import { Directive, ElementRef, Input } from '@angular/core';
import { ModalService } from '../services/modal.service';
export class ModalDirective {
    constructor(el, svc) {
        this.svc = svc;
        this.el = el;
    }
    ngOnInit() {
        this.el.nativeElement.addEventListener('click', () => { this.onClick(); });
    }
    onClick() {
        this.svc.show(this.modal);
    }
}
ModalDirective.decorators = [
    { type: Directive, args: [{
                selector: '[modal]'
            },] }
];
ModalDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: ModalService }
];
ModalDirective.propDecorators = {
    modal: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbmctY29yZS9zcmMvbGliL2RpcmVjdGl2ZXMvbW9kYWwuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBdUIsTUFBTSxlQUFlLENBQUM7QUFDbEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBS3pELE1BQU0sT0FBTyxjQUFjO0lBS3pCLFlBQ0UsRUFBYyxFQUNOLEdBQWlCO1FBQWpCLFFBQUcsR0FBSCxHQUFHLENBQWM7UUFFekIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7SUFDZixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRU8sT0FBTztRQUNiLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7WUFyQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxTQUFTO2FBQ3BCOzs7WUFMbUIsVUFBVTtZQUNyQixZQUFZOzs7b0JBTWxCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIElucHV0LCBPbkluaXQsIFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNb2RhbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9tb2RhbC5zZXJ2aWNlJztcblxuQERpcmVjdGl2ZSh7XG4gIHNlbGVjdG9yOiAnW21vZGFsXSdcbn0pXG5leHBvcnQgY2xhc3MgTW9kYWxEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBtb2RhbDogVGVtcGxhdGVSZWY8dW5rbm93bj47XG5cbiAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBlbDogRWxlbWVudFJlZixcbiAgICBwcml2YXRlIHN2YzogTW9kYWxTZXJ2aWNlXG4gICkge1xuICAgIHRoaXMuZWwgPSBlbDtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHsgdGhpcy5vbkNsaWNrKCk7IH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBvbkNsaWNrKCkge1xuICAgIHRoaXMuc3ZjLnNob3codGhpcy5tb2RhbCk7XG4gIH1cbn1cbiJdfQ==