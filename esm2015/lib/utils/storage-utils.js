import { DateTime } from 'luxon';
export class StorageUtils {
    static isExpired(item) {
        return DateTime.now().toSeconds() > item.expiration;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS11dGlscy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL25nLWNvcmUvc3JjL2xpYi91dGlscy9zdG9yYWdlLXV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxPQUFPLENBQUM7QUFFakMsTUFBTSxPQUFPLFlBQVk7SUFDaEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUEyQjtRQUNqRCxPQUFPLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxTQUFTLEVBQUUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3RELENBQUM7Q0FDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRpbWVkU3RvcmFnZUl0ZW0gfSBmcm9tIFwiLi4vbW9kZWxzL3N0b3JhZ2UvdGltZWQtc3RvcmFnZS1pdGVtXCI7XHJcbmltcG9ydCB7IERhdGVUaW1lIH0gZnJvbSAnbHV4b24nO1xyXG5cclxuZXhwb3J0IGNsYXNzIFN0b3JhZ2VVdGlscyB7XHJcbiAgcHVibGljIHN0YXRpYyBpc0V4cGlyZWQoaXRlbTogVGltZWRTdG9yYWdlSXRlbTxhbnk+KTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gRGF0ZVRpbWUubm93KCkudG9TZWNvbmRzKCkgPiBpdGVtLmV4cGlyYXRpb247XHJcbiAgfVxyXG59Il19