import * as i0 from '@angular/core';
import { Injectable, Directive, ElementRef, Input, Component, ChangeDetectorRef, ViewChild, ViewContainerRef, NgModule } from '@angular/core';
import { Subject } from 'rxjs';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { __awaiter } from 'tslib';
import { retry } from 'rxjs/operators';
import * as i1 from '@angular/common/http';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { DateTime } from 'luxon';

class ModalService {
    constructor() {
        this._onShow = new Subject();
        this.onShow = this._onShow.asObservable();
        this._onHide = new Subject();
        this.onHide = this._onHide.asObservable();
    }
    show(content) {
        this._onShow.next(content);
    }
    hide() {
        this._onHide.next();
    }
}
ModalService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ModalService_Factory() { return new ModalService(); }, token: ModalService, providedIn: "root" });
ModalService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
ModalService.ctorParameters = () => [];

class ModalDirective {
    constructor(el, svc) {
        this.svc = svc;
        this.el = el;
    }
    ngOnInit() {
        this.el.nativeElement.addEventListener('click', () => { this.onClick(); });
    }
    onClick() {
        this.svc.show(this.modal);
    }
}
ModalDirective.decorators = [
    { type: Directive, args: [{
                selector: '[modal]'
            },] }
];
ModalDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: ModalService }
];
ModalDirective.propDecorators = {
    modal: [{ type: Input }]
};

class ModalComponent {
    constructor(svc, changes) {
        this.svc = svc;
        this.changes = changes;
    }
    ngOnInit() {
        this.svc.onShow.subscribe((content) => { this.onShow(content); });
        this.svc.onHide.subscribe(() => { this.hide(); });
    }
    onShow(content) {
        this.visible = true;
        this.changes.detectChanges();
        setTimeout(() => {
            this.content.createEmbeddedView(content);
        });
    }
    onContainerClick(e) {
        if (!e.path.find(t => t.className === 'content')) {
            this.hide();
        }
    }
    hide() {
        this.visible = false;
    }
}
ModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'core-modal',
                template: "<div *ngIf=\"visible\" class=\"container\">\n  <div class=\"overlay\"></div>\n  <div (click)=\"onContainerClick($event)\" class=\"content-container\">\n    <div class=\"content\">\n      <ng-container #content></ng-container>\n    </div>\n  </div>\n</div>",
                styles: [".container,.content-container{display:flex;position:absolute;top:0;left:0;width:100vw;height:100vh}.overlay{flex-grow:1;background:black;opacity:.6}.content-container{justify-content:center;align-items:center}.content{background:white;border-radius:5px;padding:20px}\n"]
            },] }
];
ModalComponent.ctorParameters = () => [
    { type: ModalService },
    { type: ChangeDetectorRef }
];
ModalComponent.propDecorators = {
    content: [{ type: ViewChild, args: ['content', { read: ViewContainerRef },] }]
};

class NgCoreComponent {
    constructor() { }
    ngOnInit() {
    }
}
NgCoreComponent.decorators = [
    { type: Component, args: [{
                selector: 'ng-core',
                template: "<core-modal></core-modal>",
                styles: [""]
            },] }
];
NgCoreComponent.ctorParameters = () => [];

class NgCoreModule {
}
NgCoreModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ModalDirective, ModalComponent, NgCoreComponent],
                imports: [
                    CommonModule,
                    BrowserModule
                ],
                exports: [NgCoreComponent, ModalDirective, ModalComponent]
            },] }
];

class HttpService {
    constructor(http) {
        this.http = http;
        this.requests = 0;
        this._onRequest = new Subject();
        this.onRequest = this._onRequest.asObservable();
        this._onResponse = new Subject();
        this.onResponse = this._onResponse.asObservable();
    }
    handleResponse() {
        this.requests--;
        if (!this.requests)
            this._onResponse.next();
    }
    executeRequest(observable) {
        return new Promise((res, rej) => {
            this.requests++;
            this._onRequest.next();
            observable
                .pipe(retry(3))
                .subscribe((data) => {
                res(data);
                this.handleResponse();
            }, (error) => {
                rej(error);
                this.handleResponse();
            });
        });
    }
    get(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.executeRequest(this.http.get(url, {
                headers
            }));
        });
    }
    head(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.executeRequest(this.http.head(url, {
                headers
            }));
        });
    }
    getText(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.executeRequest(this.http.get(url, {
                headers,
                responseType: 'text'
            }));
        });
    }
    post(url, body, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            headers = headers || new HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            return yield this.executeRequest(this.http.post(url, body, {
                headers
            }));
        });
    }
    postUrlFormEncoded(url, body, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            headers = headers || new HttpHeaders();
            headers = headers.append('Content-Type', 'application/x-www-form-urlencoded');
            return yield this.executeRequest(this.http.post(url, body, {
                headers
            }));
        });
    }
    delete(url, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.executeRequest(this.http.delete(url, {
                headers
            }));
        });
    }
}
HttpService.ɵprov = i0.ɵɵdefineInjectable({ factory: function HttpService_Factory() { return new HttpService(i0.ɵɵinject(i1.HttpClient)); }, token: HttpService, providedIn: "root" });
HttpService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
HttpService.ctorParameters = () => [
    { type: HttpClient }
];

class StorageProvider {
    constructor(key) {
        this.key = key;
    }
}

class BrowserStorageProvider extends StorageProvider {
    constructor(key, store) {
        super(key);
        this.store = store;
    }
    get() {
        return JSON.parse(this.store.getItem(this.key));
    }
    set(value) {
        this.store.setItem(this.key, JSON.stringify(value));
    }
    remove() {
        this.store.removeItem(this.key);
    }
}

class LocalStorageProvider extends BrowserStorageProvider {
    constructor(key) {
        super(key, localStorage);
    }
}

class SessionStorageProvider extends BrowserStorageProvider {
    constructor(key) {
        super(key, sessionStorage);
    }
}

class TimedStorageItem {
    constructor(value, expiration) {
        this.value = value;
        this.expiration = expiration;
    }
}

class StorageUtils {
    static isExpired(item) {
        return DateTime.now().toSeconds() > item.expiration;
    }
}

class TimedBrowserStorageProvider extends BrowserStorageProvider {
    constructor(key, store) {
        super(key, store);
    }
    get() {
        let item = JSON.parse(this.store.getItem(this.key));
        return (StorageUtils.isExpired(item) ? null : item.value);
    }
    set(value) {
        throw '"set" not supported. Use "setValue" instead.';
    }
    setValue(value, expiresIn) {
        const expiration = DateTime.now().plus({ seconds: expiresIn }).toSeconds();
        const item = new TimedStorageItem(value, expiration);
        this.store.setItem(this.key, JSON.stringify(item));
    }
}

class TimedLocalStorageProvider extends TimedBrowserStorageProvider {
    constructor(key) {
        super(key, localStorage);
    }
}

class TimedSessionStorageProvider extends TimedBrowserStorageProvider {
    constructor(key) {
        super(key, sessionStorage);
    }
}

class ApiResponse {
}

/*
 * Public API Surface of ng-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ApiResponse, BrowserStorageProvider, HttpService, LocalStorageProvider, ModalComponent, ModalDirective, ModalService, NgCoreComponent, NgCoreModule, SessionStorageProvider, StorageProvider, StorageUtils, TimedBrowserStorageProvider, TimedLocalStorageProvider, TimedSessionStorageProvider, TimedStorageItem };
//# sourceMappingURL=ng-core.js.map
